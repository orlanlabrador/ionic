import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Nav, Platform, App, AlertController, ActionSheetController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HomePage } from '../home/home';
import { RestProvider } from '../../providers/rest/rest';
import { GlobalProvider } from '../../providers/global/global';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { MyApp } from '../../app/app.component';

/**
 * Generated class for the DesertionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-desertion',
  templateUrl: 'desertion.html',
})
export class DesertionPage {
  @ViewChild(Nav) nav: Nav;
  viewTitle='Perfil'
  userInfo: any = {};
  enterprises: any[] = [];
  peopleTypes: any[] = [];
  peopleTypes_form : any[] = [];
  peopleTypes_current: any[] = [];
  enterprise: any = {};
  loading;
  error;

  pleopleType_names_form : any[] = []
  pleopleType_names_current : any[] = []

  FormRegister: FormGroup;
  dataDidLoad: boolean;
  profilePhotoPath: string;
  imgLoaded: boolean;
  
  constructor(public navCtrl: NavController,
              public navParams: NavParams, 
              private fb: FormBuilder,
              public platform : Platform,
              public app : App,
              public alertCtrl: AlertController,
              public restProvider: RestProvider, 
              public globalProv : GlobalProvider,
              public actionSheetController : ActionSheetController,
              public camera : Camera,
              public filePath : FilePath,
              public file : File,
              public transfer : FileTransfer) {
    this.dataDidLoad = false;            
    this.init();
    this.ChangeBackButton();
  }

  crearForm(){
    
    this.updateVariablesOfView(GlobalProvider.PEOPLE_TYPE_KEY);
    //this.updateVariablesOfView(GlobalProvider.ENTERPRISE_KEY);

    this.pleopleType_names_form = []

    this.peopleTypes_form = this.peopleTypes_form.filter(element => {
      return (element.isInternal != 1);
    });
    
    this.peopleTypes_form.forEach(element => {
      this.pleopleType_names_form.push(element.name)
    });

    this.pleopleType_names_current = []
    this.peopleTypes_current.forEach(element => {
      this.pleopleType_names_current.push(element.name)
    });

    return this.fb.group({
      email:[this.userInfo.email,[Validators.required,Validators.email]],
      password:[this.userInfo.password,[Validators.minLength(8),Validators.maxLength(15)]],
      confirmpassword:[this.userInfo.password,[]],
      //enterprise:[this.enterprise,[Validators.required]],
      first_name:[this.userInfo.first_name,[Validators.required]],
      last_name:[this.userInfo.last_name,[Validators.required]],
      phone:[this.userInfo.phone,[Validators.required]],
      peopleTypes_current:[this.pleopleType_names_current,[Validators.required]]
    })
}

  async init(){
    let d = new Date();
    let loading = this.globalProv.preloader();
    loading.present();

    await this.restProvider.getData(GlobalProvider.API_PEOPLE + this.globalProv.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).id)
    .then((data:any) => {
      this.globalProv.setItemLocalStorage(GlobalProvider.PEOPLE_KEY, data.people);
    })
    .catch((err)=>{
      this.globalProv.messagesToast('No se pudo actualizar información de usuario.', undefined, undefined, "toast-warning");
    });

    /* await this.restProvider.getData(GlobalProvider.API_ENTERPRISES)
    .then((data : any) => {
      this.globalProv.setItemLocalStorage(GlobalProvider.ENTERPRISE_KEY,data.enterprises);
    })
    .catch(err => {
      this.globalProv.messagesToast('No se pudo actualizar lista de empresas.', undefined, undefined, "toast-warning");
    }); */

    await this.restProvider.getData(GlobalProvider.API_PEOPLE_TYPES)
    .then((data : any) =>{
      this.globalProv.setItemLocalStorage(GlobalProvider.PEOPLE_TYPE_KEY,data.peopletypes);
    })
    .catch(err =>{
      this.globalProv.messagesToast('No se pudo actualizar la lista de los roles.', undefined, undefined, "toast-warning");
    });

    this.userInfo = this.globalProv.getItemLocalStorage(GlobalProvider.PEOPLE_KEY);
    if(this.userInfo.photo==null){
      MyApp._user.imgLoaded = true;
      MyApp._user.urlPhoto = "assets/imgs/dummy_profile.jpg";
    }else{
      MyApp._user.imgLoaded = false;
      MyApp._user.urlPhoto = RestProvider.BASE_URL_SERVER + '/storage/people/' + this.userInfo.id + "-photo/" + this.userInfo.photo + '?a=' + d.getTime();
      setTimeout(() => {
        if(!this.imgLoaded){
          MyApp._user.urlPhoto = RestProvider.BASE_URL_SERVER + '/storage/people/' + this.userInfo.id + "-photo/" + this.userInfo.photo;
          this.profilePhotoPath = MyApp._user.urlPhoto;
          MyApp._user.imgLoaded = true;
          this.imgLoaded = MyApp._user.imgLoaded;
        }
      }, 20000);
    }
    this.imgLoaded = MyApp._user.imgLoaded;
    this.profilePhotoPath = MyApp._user.urlPhoto;

    this.FormRegister=this.crearForm();
    console.log("Form", this.FormRegister);
    this.dataDidLoad = true;

    loading.dismiss();
  }

  updateVariablesOfView(key : string){
   if(key == GlobalProvider.ENTERPRISE_KEY){
      this.enterprises = this.globalProv.getItemLocalStorage(GlobalProvider.ENTERPRISE_KEY);
      this.enterprises.forEach(element => {
        if (this.userInfo.enterprise_id == element.id){
          this.enterprise = element;
         }
       });
    }else{
      this.peopleTypes = this.globalProv.getItemLocalStorage(GlobalProvider.PEOPLE_TYPE_KEY);
      this.peopleTypes_form = []
      this.peopleTypes.forEach(element => {
        this.peopleTypes_form.push(element)
      });
      let peopleTypes_curr = this.userInfo.people_types;
      peopleTypes_curr.forEach(element => {
        this.peopleTypes_current.push(element)
      });
    }
  }

  async createAccount(){
    let roles = [];
    this.FormRegister.value.peopleTypes_current.forEach(element => {
      roles.push(this.peopleTypes_form.find(peop_curr =>{
        return peop_curr.name == element
      }).id)
    });

    let values = this.FormRegister.value
    let newUser = {
        email : values.email,
        password: values.password,
        //enterprise_id: values.enterprise.id,
        enterprise_id: this.userInfo.enterprise.id,
        first_name: values.first_name,
        last_name: values.last_name,
        phone: values.phone,
        peopleTypes: roles
    }

    let loading = this.globalProv.preloader()
    loading.present();

     await this.restProvider.postData(GlobalProvider.API_EDIT_PROFILE + this.userInfo.id, newUser)
      .then((data:any) => {
        this.globalProv.messagesToast('Usuario modificado', undefined, undefined, "toast-success");
      })
      .catch((err)=>{
        if (err.status==401){
          this.globalProv.messagesToast('El correo que intenta ingresar ya esta asignado a otro usuario', undefined, undefined, "toast-error")
        } else {
          this.globalProv.messagesToast('Servicio temporalmente no disponible', undefined, undefined, "toast-error")
        }
      });
      await this.globalProv.updateUserInfoApi();
      loading.dismiss();
  }

  loadEnded(){
    this.imgLoaded = true;
  }

  async profilePhoto(){
    const actionSheet = await this.actionSheetController.create({
      title: "Seleccione fuente",
      buttons: [{
              text: 'Buscar en galería',
              handler: () => {
                  this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
              }
              },
              {
                  text: 'Usar cámara',
                  handler: () => {
                      this.takePicture(this.camera.PictureSourceType.CAMERA);
                  }
              },
              {
                  text: 'Cancelar',
                  role: 'cancel'
              }
          ]
      });
    await actionSheet.present();
  }

  takePicture(sourceType: PictureSourceType) {
    let options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.FILE_URI,
        sourceType: sourceType,
        saveToPhotoAlbum: false,
        correctOrientation: true,
        targetWidth: 400,
        targetHeight: 400,
        mediaType: this.camera.MediaType.PICTURE,
        encodingType: this.camera.EncodingType.JPEG
    };
    
    this.camera.getPicture(options).then(imagePath => {
        if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
            this.filePath.resolveNativePath(imagePath)
                .then(filePath => {
                    let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    this.uploadPhoto(filePath, currentName);
                }, (error) => {
                });
        } else {
            var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            this.uploadPhoto(imagePath, currentName);
        }
    }, error => {
    });
  }


  private uploadPhoto(imageFileUri: any, nameFile : string) {
    let d = new Date();
    MyApp._user.imgLoaded = false;
    this.imgLoaded = MyApp._user.imgLoaded;
    const fileTransfer: FileTransferObject = this.transfer.create();
  
    let options: FileUploadOptions = {
      fileKey: 'photo',
      fileName: nameFile,
      chunkedMode: false,
      mimeType: "image/jpeg",
      headers: {}
    }
    fileTransfer.upload(imageFileUri, RestProvider.API_URL + GlobalProvider.API_UPLOAD_PROFILE_PHOTO + this.globalProv.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).id, options)
    .then((data) => {
      MyApp._user.urlPhoto = RestProvider.BASE_URL_SERVER + '/storage/people/' + this.userInfo.id + "-photo/" + this.userInfo.photo + '?a=' + d.getTime();
      this.profilePhotoPath = MyApp._user.urlPhoto;
      this.globalProv.messagesToast('Se ha editado la imágen de perfil', undefined, undefined, 'toast-success');
    }, (err) => {
      MyApp._user.imgLoaded = true;
      this.imgLoaded = MyApp._user.imgLoaded;
      this.globalProv.messagesToast('No se pudo modificar la foto de perfil', undefined, undefined, 'toast-error');
    });

  }

  ChangeBackButton(){
    this.platform.registerBackButtonAction(() => {
      let nav = this.app.getActiveNavs()[0];
      nav.setRoot(HomePage,{},{animate: true, direction: 'backward'});
  });
  }

}