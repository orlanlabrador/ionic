import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Platform, App, Nav } from 'ionic-angular';
import { InspectionDetailsPage } from '../inspectionDetails/inspectionDetails';
import { RestProvider } from '../../providers/rest/rest';
import { GlobalProvider } from '../../providers/global/global';
import { IonicSelectableComponent } from 'ionic-selectable';

@Component({
  selector: 'page-inspection',
  templateUrl: 'inspection.html'
})
export class InspectionPage {
  @ViewChild(Nav) nav: Nav;
  @ViewChild('selectSearchComponent') selectSearchComponent: IonicSelectableComponent;

  planification_id;
  register: any=[]
  inspection: any = {}
  inspectionInform: any = {}
  peoplesFix : any[] = [];

  inspectionInProgress: any = {}

  //Variables para controlar la vista
  dataDidLoad: boolean;
  
  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public restProvider : RestProvider,
              public platform : Platform,
              public app : App,
              public globalProvider: GlobalProvider) {
    this.inspection=navParams.get('item');
    this.dataDidLoad = false;
    this.ChangeBackButton();
    this.init();
  }

  async init(){
    let loading = this.globalProvider.preloader();
    loading.present();
    if (this.globalProvider.existsItemLocalStorage(GlobalProvider.PLANIFICATIONS_CURRENT_KEY)){
      this.inspectionInProgress = this.globalProvider.getItemLocalStorage(GlobalProvider.PLANIFICATIONS_CURRENT_KEY);
      console.log("Retomando inspection in progress",this.inspectionInProgress);
    }else{
      await this.restProvider.getData(GlobalProvider.API_PEOPLES)
        .then((data : any) => {
          this.globalProvider.setItemLocalStorage(GlobalProvider.ALL_PEOPLES, data.peoples)
        })
        .catch(err => {
          this.globalProvider.messagesToast('No se pudo actualizar lista de personas.', undefined, undefined, "toast-warning")
        })
      this.inspectionInProgress.peoples = this.globalProvider.getItemLocalStorage(GlobalProvider.ALL_PEOPLES);
      this.inspectionInProgress.rolSelected = {};
      this.inspectionInProgress.rolSelectedAux = {};
      this.inspectionInProgress.inspectionInform = {};
      this.inspectionInProgress.inspectionInform.leader = this.navParams.get('peoples').find(people=>{return (people.pivot.leader === 1);});
      this.inspectionInProgress.inspectionInform.planification_id = this.inspection.id;
      this.inspectionInProgress.inspectionInform.inspection_id = this.navParams.get("id");
      this.inspectionInProgress.roles = this.globalProvider.getItemLocalStorage(GlobalProvider.PEOPLE_TYPE_KEY);
      this.inspectionInProgress.peoples = this.globalProvider.getItemLocalStorage(GlobalProvider.ALL_PEOPLES);
      this.inspectionInProgress.inspectionInform.peoples_present = [];
      this.inspection.peoples.forEach(peopleInspec => {
        this.inspectionInProgress.roles = this.inspectionInProgress.roles.filter(ele=>{
          return (ele.id !== peopleInspec.pivot.people_type_id && ele.isInternal === 0);
        });
        this.inspectionInProgress.peoples = this.inspectionInProgress.peoples.filter(people=>{
          return (people.id !== peopleInspec.id);
        });
        peopleInspec.name = peopleInspec.first_name + ' ' + peopleInspec.last_name;
        peopleInspec.selected_peopleType = peopleInspec.people_types.find(people_type => {
          return (people_type.id === peopleInspec.pivot.people_type_id);
        });
        if(peopleInspec.pivot.leader !== 1){
          this.inspectionInProgress.inspectionInform.peoples_present.push(peopleInspec);
        }
      });

      console.log("Personas disponibles para la obra", this.inspection.peoples);

      this.globalProvider.setItemLocalStorage(GlobalProvider.PLANIFICATIONS_CURRENT_KEY,this.inspectionInProgress);
      console.log("Iniciando inspection in progress",this.inspectionInProgress);
    }
    this.dataDidLoad = true;
    loading.dismiss();
  }

  ChangeBackButton(){
    this.platform.registerBackButtonAction(() => {
      let nav = this.app.getActiveNavs()[0];
      nav.pop({animate: true, direction: 'backward'});
  });
  }

  goViewInspectionDetails(){
    this.navCtrl.push(InspectionDetailsPage, {inspectionInform:this.inspectionInProgress.inspectionInform,inspection:this.inspection}, {animate: true, direction: 'forward'});
  }

  searchNames() {
    this.inspectionInProgress.rolSelected = this.inspectionInProgress.rolSelected;
    this.inspectionInProgress.names = [];
    this.inspectionInProgress.peoples.forEach(people => {
      if (people.people_types.find((people_type:any) => {
        return people_type.id == this.inspectionInProgress.rolSelected.id;
      })){
        people.name = people.first_name + ' ' + people.last_name;
        this.inspectionInProgress.names.push(people);
      }
    });
    this.globalProvider.setItemLocalStorage(GlobalProvider.PLANIFICATIONS_CURRENT_KEY,this.inspectionInProgress);
  }

  onSelect(event: {
    component: IonicSelectableComponent,
    item: any,
    isSelected: boolean
  }) {
    let selected = event.item;
    selected.selected_peopleType = this.inspectionInProgress.rolSelected;
    this.inspectionInProgress.inspectionInform.peoples_present.push(selected);
    this.inspectionInProgress.peoples.splice(this.inspectionInProgress.peoples.findIndex(people=>{return (people.id===selected.id);}), 1);
    this.inspectionInProgress.roles.splice(this.inspectionInProgress.roles.findIndex(rol=>{return (rol.id===this.inspectionInProgress.rolSelected.id);}), 1);
    this.inspectionInProgress.rolSelected = {};
  }

  onClose(event: { component: IonicSelectableComponent }) {
    this.inspectionInProgress.participantSelected = {};
    this.clearSelectSearch();
  }

  clearSelectSearch() {
    this.selectSearchComponent.clear();
    this.globalProvider.setItemLocalStorage(GlobalProvider.PLANIFICATIONS_CURRENT_KEY,this.inspectionInProgress);
  }

  removeSelected(people_present : any){
    this.inspectionInProgress.roles.push(people_present.selected_peopleType);
    delete people_present.selected_peopleType;
    this.inspectionInProgress.peoples.push(people_present);
    this.inspectionInProgress.inspectionInform.peoples_present.splice(this.inspectionInProgress.inspectionInform.peoples_present.findIndex((people=>{return (people.id===people_present.id);})), 1);
    this.globalProvider.setItemLocalStorage(GlobalProvider.PLANIFICATIONS_CURRENT_KEY,this.inspectionInProgress);
  }

}


