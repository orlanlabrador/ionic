import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Nav, Platform, App } from 'ionic-angular';

@Component({
  selector: 'page-preview-inspection',
  templateUrl: 'previewInspection.html',
})
export class PreviewInspectionPage {
  @ViewChild(Nav) nav: Nav;

  report : any = {};
  observations_serious : any[] = []
  observations_secondary : any[] = []
  observationIds : any[] = []
  observations: any [] = [];
  peoples: any [] = [];
  observationsForTheView_aux : any[] = [];
  observationsForTheView : any[] = [];
  observationTypes : any[] = [];
  peoples_presents : any[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public platform : Platform,
              public app : App) {
    this.report = this.navParams.get('report')
    this.peoples = this.report.peoples;

    this.report.reports.forEach(report => {
      report.observation.evaluations = [];
      if(!this.observationIds.find(observationid => {return (observationid == report.observation_id)})){
        this.observationIds.push(report.observation_id);
      }
    });

    this.observationIds.forEach(observationId => {
      let observation : any = {};
      let firtsIteration : boolean = true;
      this.report.reports.forEach(report => {
        if(observationId == report.observation_id){
          if(firtsIteration){
            observation = report.observation;
            firtsIteration = false;
          }
          report.evaluation.option = report.option;
          report.evaluation.comment = report.comment;
          observation.evaluations.push(report.evaluation);
        }
      });
      this.observations.push(observation);
    });

    this.observations.forEach(observation => {
      if(observation.observation_type_id == 1){
        this.observations_serious.push(observation)
      } else {
        this.observations_secondary.push(observation)
      }
    })

    this.report.reports.filter(rep=>{
      return rep.observation.evaluations.length != 0;
    }).forEach(observation => {
      this.observationsForTheView_aux.push(observation.observation);
    });

    this.observationsForTheView_aux.forEach((item) => {
      if(!this.observationTypes.find(observationType => {return (observationType.id == item.observation_type_id)})){
        this.observationTypes.push(item.observation_type);
      }
    })
    this.observationTypes.forEach(obsType => {
      let firstIteration : boolean = true;
      this.observationsForTheView_aux.forEach(observation => {
        if(observation.observation_type_id == obsType.id){
          observation.IsFirst = firstIteration;
          if(firstIteration){
            firstIteration = false;
          }
          this.observationsForTheView.push(observation);
        }
      });
    });

    console.log(this.observationsForTheView)

    this.peoples.forEach(peopleInspec => {
      peopleInspec.name = peopleInspec.first_name + ' ' + peopleInspec.last_name;
      peopleInspec.people_type = peopleInspec.people_types.find(people_type => {
        return (people_type.id === peopleInspec.pivot.people_type_id);
      })
      this.peoples_presents.push(peopleInspec);
    });
    
    this.ChangeBackButton();
  }

  ChangeBackButton(){
    this.platform.registerBackButtonAction(() => {
      this.goBack();
    });
  }

  goBack(){
    this.navCtrl.pop();
  }

}
