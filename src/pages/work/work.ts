import { RestProvider } from './../../providers/rest/rest';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Nav, Platform, App } from 'ionic-angular';
import { EditworkPage } from '../editwork/editwork';
import { FormBuilder  } from '@angular/forms';
import { HomePage } from '../home/home';
import { GlobalProvider } from '../../providers/global/global';
import { NewWorkPage } from '../newWork/newWork';


@Component({
  selector: 'page-work',
  templateUrl: 'work.html',
})
export class WorkPage {
  @ViewChild(Nav) nav: Nav;

  obras : any[] = [];
  userInfo: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public fb: FormBuilder,
              public globalProv : GlobalProvider,
              public restProvider: RestProvider,
              public platform : Platform,
              public app : App) {
    this.userInfo = this.globalProv.getItemLocalStorage(GlobalProvider.PEOPLE_KEY);
    console.log(this.userInfo)
    this.ChangeBackButton();
  }

  ChangeBackButton(){
    this.platform.registerBackButtonAction(() => {
      // Catches the active view
      let nav = this.app.getActiveNavs()[0];             
      // Checks if can go back before show up the alert
      nav.setRoot(HomePage,{},{animate: true, direction: 'backward'});
    });
  }

  ionViewDidEnter(){
    let loading = this.globalProv.preloader();
    loading.present();

    this.restProvider.getData(GlobalProvider.API_CONSTRUCTIONS_PER_PEOPLE + this.globalProv.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).id)
    .then((data:any) => {
      this.globalProv.setItemLocalStorage(GlobalProvider.CONSTRUCTIONS_KEY, data.people[0].constructions)
      this.obras = this.globalProv.getItemLocalStorage(GlobalProvider.CONSTRUCTIONS_KEY);
      // this.obras = this.obras.filter(obra => {
      //   return obra.status == "Confirmada";
      // });
      console.log("Obras", this.obras);
      loading.dismiss();
    })
    .catch((err)=>{
      this.globalProv.messagesToast('No se pudo actualizar la lista de obras', undefined, undefined, "toast-warning")
      loading.dismiss();
    });
    this.obras = this.globalProv.getItemLocalStorage(GlobalProvider.CONSTRUCTIONS_KEY);
    this.obras = this.obras.filter(obra => {
      return obra.status == "Confirmada";
    });
    console.log("Obras", this.obras);
  }

  addWork(){
    this.navCtrl.push(NewWorkPage, {animate: true, direction: 'forward'})
  }

  goViewEditwork(obra : any){
    this.navCtrl.push(EditworkPage, {obra : obra}, {animate: true, direction: 'forward'});
  }

}
