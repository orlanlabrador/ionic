import { RestProvider } from './../../providers/rest/rest';
import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GlobalProvider } from '../../providers/global/global';
import { StartPage } from '../start/start';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  peopleTypes=[]
  enterprises=[]
  FormRegister: FormGroup;


  constructor(public fb: FormBuilder, 
              public navCtrl: NavController, 
              public navParams: NavParams, /* public device : Device, */ 
              public restProvider : RestProvider,
              public menuCtrl: MenuController,
              public globalProvider : GlobalProvider) {
    this.ChangeBackButton();
    this.peopleTypes=navParams.get('peopleTypes');
    this.enterprises=navParams.get('enterprises');
    this.menuCtrl.enable(false, 'MenuObras');
    this.FormRegister=this.crearForm();
  }
  
  ChangeBackButton(){
    this.globalProvider.platform.registerBackButtonAction(() => {
      let nav = this.globalProvider.app.getActiveNavs()[0];
      nav.setRoot(StartPage,{},{animate: true, direction: 'backward'});
  });
  }

  crearForm(){
      return this.fb.group({
        email:['',[Validators.required,Validators.email]],
        password:['',[Validators.required,Validators.minLength(8),Validators.maxLength(15)]],
        confirmpassword:['',[Validators.required]],
        enterprise_id:['',[Validators.required]],
        first_name:['',[Validators.required]],
        last_name:['',[Validators.required]],
        phone:['',[Validators.required]],
        peopleTypes:['',[Validators.required]]
      })
  }

  prueba(){
    console.log("Si toma el evento de enter");
  }

  createAccount(){
    let roles = [];
    this.FormRegister.value.peopleTypes.forEach(element => {
      roles.push(element.id)
    });
    
    let values = this.FormRegister.value
    let newUser = {
        email : values.email,
        password: values.password,
        enterprise_id: values.enterprise_id.id,
        first_name: values.first_name,
        last_name: values.last_name,
        phone: values.phone,
        status: "Pendiente",
        peopleTypes: roles,
        type : "Externo"
    }

    console.log("Nuevo usuario", newUser);

    let loading = this.globalProvider.preloader()
    loading.present();
      this.restProvider.postData(GlobalProvider.API_REGISTER, newUser)
      .then((data:any) => {
        this.globalProvider.messagesToast('Usuario registrado', undefined, undefined, "toast-success")
        loading.dismiss();
        this.navCtrl.pop({animate : true, direction: 'backward'})
      })
      .catch((err)=>{
        if (err.status==401){
          this.globalProvider.messagesToast('El correo que intenta registrar ya existe', undefined, undefined, "toast-error")
        } else {
          this.globalProvider.messagesToast('Servicio temporalmente no disponible', undefined, undefined, "toast-error")
        }
        loading.dismiss();
      });
  }

  goBack(){
    this.navCtrl.pop({animate : true, direction: 'backward'})
  }

  showTermAndConditions(){
    // Term and Condition
  }

  showPrivacyPolicy(){
    // Privacy Policy
  }
}
