import { RestProvider } from './../../providers/rest/rest';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Nav, Platform, App } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GlobalProvider } from '../../providers/global/global';
import { PermissionProvider } from '../../providers/permissions/permissions';

declare var google;
@Component({
  selector: 'page-new-work',
  templateUrl: 'newWork.html',
})
export class NewWorkPage {
  @ViewChild(Nav) nav: Nav;

  myForm: FormGroup;
  email : string;

  latitude : number;
  longitude : number;

  enterprises : any[] = [];
  departments : any[] = [];
  provinces : any[] = [];
  districts : any[] = [];
  minDate : string;
  maxDate : string;
  userRoles: any[] = [];
  userRolesNames : any[] = [];

  map: any;
  subsToValidateDatesStartDate: any;
  subsToValidateDatesEndDate: any;
  validTime : any = { 
    validDates: true
  };
  userInfo: any;
  dataDidLoad: boolean;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public fb: FormBuilder,
              public restProvider: RestProvider,
              public platform : Platform,
              public app : App,
              public globalProvider : GlobalProvider,
              public permitProvider : PermissionProvider) {
    this.dataDidLoad = false;                
    this.init();
  }

  crearFormulario(){
    return this.fb.group({
      name: ['', [Validators.required]],
      address: ['', [Validators.required]],
      start_date: ['', [Validators.required]],
      end_date: ['', [Validators.required]],
      enterprise_id:['',[Validators.required]],
      district:['',[Validators.required]],
      province:['',[Validators.required]],
      department:['',[Validators.required]],
      zip_code:['',[Validators.required]],
      selected:['',[Validators.required]]
    });
  }

  ionViewWillLeave(){
    if(this.dataDidLoad){
      this.subsToValidateDatesStartDate.unsubscribe();
      this.subsToValidateDatesEndDate.unsubscribe();
    }
  }

  initiateUserInfo(){
    this.userInfo = this.globalProvider.getItemLocalStorage(GlobalProvider.PEOPLE_KEY);
    if(this.userInfo.enterprise_id != 2){
      this.myForm.controls['enterprise_id'].setValue(this.userInfo.enterprise_id);
    }
    this.userRoles = [];
    this.userRolesNames = [];
    this.userRoles = this.userInfo.people_types;
    this.userRoles = this.userRoles.filter(rol => {
      return rol.isSupervisor == 1;
    });
    this.userRoles.forEach(rol => {
      this.userRolesNames.push(rol.name);
    });
  }

  async init(){
    let loading = this.globalProvider.preloader();
    loading.present();

    let enterpriseIsLoad : boolean = true

    await this.restProvider.getData(GlobalProvider.API_ENTERPRISES)
      .then((data : any) => {
        this.globalProvider.setItemLocalStorage(GlobalProvider.ENTERPRISE_KEY,data.enterprises);
      })
      .catch(err => {
        enterpriseIsLoad = false
        this.globalProvider.messagesToast('No se pudo actualizar lista de empresas.', undefined, undefined, "toast-warning")
      });

    await this.restProvider.getData(GlobalProvider.API_DEPARTAMENT)
        .then((data:any) => {
          this.globalProvider.setItemLocalStorage(GlobalProvider.ALL_DEPARTMENTS, data.departments);
        })
        .catch((err) => {
          this.globalProvider.messagesToast('No se pudo actualizar la lista de departamentos', undefined, undefined, "toast-warning")
        });

      await this.restProvider.getData(GlobalProvider.API_PEOPLE + this.globalProvider.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).id)
      .then((data : any) => {
        this.globalProvider.setItemLocalStorage(GlobalProvider.PEOPLE_KEY, data.people);
      })
      .catch((err) => {
        this.globalProvider.messagesToast('No se pudo actualizar la información del usuario', undefined, undefined, "toast-warning")
      });

      this.departments = this.globalProvider.getItemLocalStorage(GlobalProvider.ALL_DEPARTMENTS);
      [this.minDate, this.maxDate] = this.globalProvider.CurrentAndNextdate();
      this.myForm = this.crearFormulario();
      if (!enterpriseIsLoad){
        this.globalProvider.messagesToast('Servicio temporalmente no disponible', undefined, undefined, "toast-error");
        loading.dismiss();
        return;
      }
      this.enterprises = this.getEnterprises(this.globalProvider.getItemLocalStorage(GlobalProvider.ENTERPRISE_KEY));
      this.initiateUserInfo();
      [this.subsToValidateDatesStartDate, this.subsToValidateDatesEndDate] = this.globalProvider.changeIniDateAndSetFinDate(this.myForm, ['start_date','end_date'], this.validTime, false);
      this.dataDidLoad = true;

      //Latitud y longitud de Lima, Peru
      this.latitude = -12.046261;
      this.longitude = -77.042756;

      setTimeout(() => {
        this.loadMap();
        loading.dismiss();
      }, 0);

      this.getPosition();
  }

  getEnterprises(enterprises : any[]): any[]{
    return enterprises.filter((entrerprise : any) =>{
      return entrerprise.id != 2;
    })
  }

  async getPosition(){

    await this.permitProvider.requestPermissions(0);

    let isLocationAccracy : boolean = await this.permitProvider.requestForAccuracy(true);

    let positionPromise : any;
    if (isLocationAccracy){
      positionPromise = await this.globalProvider.obtainCurrentPosition();
      console.log("Position promise returned", positionPromise);
      if(positionPromise.isPosition){
        this.latitude = positionPromise.latitude;
        this.longitude = positionPromise.longitude;
        this.loadMap();
      }
    }

  }

  loadMap(){
    let latitude = this.latitude;
    let longitude = this.longitude;
    
    // create a new map by passing HTMLElement
    let mapEle: HTMLElement = document.getElementById('map');
  
    // create LatLng object
    let myLatLng = {lat: latitude, lng: longitude};
  
    // create map
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 14
    });

    let marker = new google.maps.Marker({
      position: myLatLng,
      map: this.map,
    });

    let geocoder = new google.maps.Geocoder;
    let infowindow = new google.maps.InfoWindow;

    this.getDescription(geocoder, myLatLng, infowindow, marker);
    
  
    google.maps.event.addListener(this.map, 'click', (e) => {
      marker.setPosition(e.latLng)
      this.latitude = e.latLng.lat();
      this.longitude = e.latLng.lng();
      let latlong = {lat: this.latitude, lng: this.longitude}
      infowindow.close();
      this.getDescription(geocoder, latlong, infowindow, marker)
    });
  }

  getDescription(geocoder, latlong, infowindow, marker){
    geocoder.geocode({'location': latlong}, function(results, status) {
      if (status === 'OK' && results[5]) {
        console.log(results[5])
        infowindow.setContent(results[5].formatted_address);
        infowindow.open(this.map, marker);
      } else {
        console.log('Geocoder failed due to: ' + status);
      }
    });
  }

  backWork(){
    this.navCtrl.pop({animate: true, direction: 'backward'})
  }

  metodoparaver(){
    console.log("Formulario entero en ionselect event",this.myForm.value);
  }

  saveNewWork(){
    let loading = this.globalProvider.preloader()
    loading.present();
    console.log("latitud",this.latitude);
    console.log("longitud",this.longitude);

    let enterprise_id;

    if(this.userInfo.enterprise_id == 2){
      enterprise_id = this.myForm.value.enterprise_id.id;
    }else{
      enterprise_id = this.userInfo.enterprise_id;
    }

    let newObra = {
      enterprise_id : enterprise_id,
      description : this.myForm.value.name,
      address : this.myForm.value.address,
      district_id : this.myForm.value.district.id,
      province_id : this.myForm.value.province.id,
      department_id : this.myForm.value.department.id,
      ubigeo: this.myForm.value.department.code + this.myForm.value.province.code + this.myForm.value.district.code,
      latitude : this.latitude,
      longitude : this.longitude,
      zip_code : this.myForm.value.zip_code,
      start_date : this.myForm.value.start_date,
      end_date : this.myForm.value.end_date,
      status : "Pendiente",
      selected : [
        {
          peopleTypes : { 
            id : this.userInfo.people_types.find(peopletype => {
              return (peopletype.name == this.myForm.value.selected);
            }).id
          },
          peoples : {
            id : this.userInfo.id
          }
        }
      ]
    }

    console.log("Nueva obra por enviar", newObra);

    this.restProvider.postData(GlobalProvider.API_CONSTRUCTIONS_CRUD, newObra)
    .then((data:any) => {
      this.globalProvider.messagesToast("Se agregó la obra", undefined, undefined, "toast-success")
      loading.dismiss();
      this.navCtrl.pop({animate : true, direction: 'backward'});
    })
    .catch((err)=>{
      this.globalProvider.messagesToast('Servicio temporalmente no disponible', undefined, undefined, "toast-error")
      loading.dismiss();
    });
  }

  onDepartChange(): void {
    let department = this.myForm.get('department').value;
    this.restProvider.getData(GlobalProvider.API_DEPARTAMENT + "/" + department.id).then((data : any) =>{
      this.provinces = data.department.provinces;
      this.myForm.value.province = '';
      this.myForm.value.district = '';
    }).catch(err=>{
      this.provinces = [];
      console.log("error", err);
      this.globalProvider.messagesToast("No se pudo actualizar la lista de provincias", undefined, undefined, "toast-warning")
    })
    this.districts = [];
  }

  onProvinChange(): void {
    let province = this.myForm.get('province').value;
    this.myForm.value.district = '';
    this.districts = this.provinces.find(provin =>{
      return provin.id == province.id;
    }).districts;
  }

  onDistirtChange(): void {
  }

}
