import { RestProvider } from './../../providers/rest/rest';
import { Component } from '@angular/core';
import { MenuController, NavController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginPage } from '../login/login';
import { GlobalProvider } from '../../providers/global/global';

@Component({
  selector: 'page-recovery-password',
  templateUrl: 'recoveryPassword.html',
})
export class RecoveryPasswordPage {

    FormPassword: FormGroup;

    constructor(public navCtrl : NavController,
                public fb: FormBuilder,
                public restProvider: RestProvider,
                public global: GlobalProvider,
                public menuCtrl : MenuController) {
        this.ChangeBackButton();
        this.menuCtrl.enable(false, 'MenuObras');
        this.FormPassword=this.crearFormulario();
    }

    ChangeBackButton(){
      this.global.platform.registerBackButtonAction(() => {
        let nav = this.global.app.getActiveNavs()[0];
        nav.setRoot(LoginPage,{},{animate: true, direction: 'backward'});
    });
    }

    crearFormulario(){
        return this.fb.group({
          email_recovery: ['', [Validators.required, Validators.email]]
        });
      }

    recoveryPassword(){
      let loading = this.global.preloader()
      loading.present();

      this.restProvider.postData("auth/recuperar-password/" + this.FormPassword.value.email_recovery, {email : this.FormPassword.value.email_recovery}).then((data : any) => {
          this.global.messagesToast('Se ha enviado un correo con su nueva contraseña', undefined, undefined, "toast-success")
          loading.dismiss();
          this.navCtrl.push(LoginPage);
        })
        .catch((err)=>{
          if (err.status==401){
            this.global.messagesToast('El correo no existe', undefined, undefined, "toast-error")
          } else {
            this.global.messagesToast('Servicio temporalmente no disponible', undefined, undefined, "toast-error")
          }
          loading.dismiss();
          console.log("CATCH ERROR DATA URL",err);
        });
    }

    goBack(){
      this.navCtrl.pop({animate : true, direction: 'backward'})
    }
}