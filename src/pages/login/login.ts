import { RestProvider } from './../../providers/rest/rest';
import { Component } from '@angular/core';
import { MenuController, NavController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { RecoveryPasswordPage } from '../recoveryPassword/recoveryPassword';
import { GlobalProvider } from '../../providers/global/global';
import { HomePage } from '../home/home';
import { StartPage } from '../start/start';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

    FormLogin: FormGroup;

    constructor(public navCtrl : NavController,
                public fb: FormBuilder,
                public restProvider: RestProvider,
                public menuCtrl: MenuController,
                public globalProv : GlobalProvider
                ) {
        this.ChangeBackButton();
        this.menuCtrl.enable(false, 'MenuObras');
        this.FormLogin=this.crearFormulario();
    }

    ChangeBackButton(){
      this.globalProv.platform.registerBackButtonAction(() => {
        let nav = this.globalProv.app.getActiveNavs()[0];
        nav.setRoot(StartPage,{},{animate: true, direction: 'backward'});
    });
    }

    crearFormulario(){
        return this.fb.group({
          email: ['', [Validators.required, Validators.email]],
          password: ['', [Validators.required]],
        });
      }

    login(){
      let loading = this.globalProv.preloader();
      loading.present();
      this.restProvider.postData(GlobalProvider.API_LOGIN, this.FormLogin.value)
      .then((data:any) => {
        this.globalProv.setLocalStorageData(data);
        this.navCtrl.setRoot(HomePage,{},{animate: true, direction: 'forward'});
        loading.dismiss();
      })
      .catch((err)=>{
        if (err.status==401){
          this.globalProv.messagesToast('El usuario no existe o aún no ha sido autorizado', undefined, undefined, "toast-error")
        } else {
          this.globalProv.messagesToast('Servicio temporalmente no disponible', undefined, undefined, "toast-error")
        }
        loading.dismiss();
        console.log("CATCH ERROR DATA URL",err);
      })
    }

    goRecoveryPassword(){
      this.navCtrl.push(RecoveryPasswordPage)
    }

    goBack(){
      this.navCtrl.pop({animate : true, direction: 'backward'})
    }
}