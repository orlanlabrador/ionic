import { Component } from '@angular/core';
import { NavController, ViewController, NavParams } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'page-comment',
  templateUrl: 'comment.html'
})
export class CommentPage {
  comment : FormGroup;
  savebutton : string = '';
  commentnull : boolean = false;

  constructor(public navCtrl: NavController,
              public navParams : NavParams,
              public viewCtrl: ViewController,
              public formb: FormBuilder) {
    this.comment = this.formb.group({
      comentario: [this.navParams.get('existentcomment'),Validators.required]
    });
    if(this.comment.value.comentario == null){
      this.savebutton = 'save-comment';
      this.commentnull = true;
    }else{
      this.savebutton = 'save-comment-with-delete';
    }
    this.comment.value.comentario = this.navParams.get('existentcomment');
  }

  comentar(){
    let comment = this.comment.value.comentario;
    this.viewCtrl.dismiss(comment);
  }

}
