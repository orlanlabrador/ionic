import { RestProvider } from './../../providers/rest/rest';
import { Component } from '@angular/core';
import { NavController, NavParams, Platform, App, AlertController } from 'ionic-angular';
import { InspectionPage } from '../inspection/inspection';
import { HomePage } from '../home/home';
import { GlobalProvider } from '../../providers/global/global';
import { NewPlanificationPage } from '../newPlanification/newPlanification';
import { PermissionProvider } from '../../providers/permissions/permissions';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  
  people : any;
  inspections : any[];
  register: any=[]
  athete: any = {}
  isInspectionInProcess: boolean = false;

  typeUser: string = ''
  updated: boolean;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public platform : Platform,
              public app : App,
              public restProvider : RestProvider,
              public alertCtrl : AlertController,
              private globalProvider: GlobalProvider,
              public permitProvider : PermissionProvider) {
    GlobalProvider.MEDIA_ARRAY = this.globalProvider.getItemLocalStorage(GlobalProvider.MEDIA_ARRAY_KEY);
    this.typeUser = this.globalProvider.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).type;
    this.ChangeBackButton();
  }

  ChangeBackButton(){
    this.platform.registerBackButtonAction(() => {
      // Catches the active view
      let nav = this.app.getActiveNavs()[0];
      // Checks if can go back before show up the alert
      nav.setRoot(HomePage,{},{animate: true, direction: 'backward'});
    });
  }

  async ionViewDidEnter(){
    let loading = this.globalProvider.preloader();
    let inspectionsaux;
    loading.present();
    this.isInspectionInProcess = false;

    await this.restProvider.getData(GlobalProvider.API_PLANIFICATIONS_PER_PEOPLE + this.globalProvider.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).id)
    .then((data : any) => {

      // Determina si es el lider de la planificación
      let planifications : any[] = []
      if (data.people[0].planifications.length !== 0){
        data.people[0].planifications.forEach(plan=>{
          if (plan.peoples.find(people=>{
            return people.pivot.leader === 1 && people.pivot.people_id === this.globalProvider.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).id;
          }) && plan.status != 'Terminado'){
            planifications.push(plan);
          }
        })
      }

      this.globalProvider.setItemLocalStorage(GlobalProvider.PLANIFICATIONS_KEY,planifications);
      this.updated = true;
    })
    .catch(err => {
      this.globalProvider.messagesToast("No se pudieron actualizar las inspecciones.",undefined,undefined,"toast-warning");
      this.updated = false;
    })
    this.inspections = this.globalProvider.getItemLocalStorage(GlobalProvider.PLANIFICATIONS_KEY);
    if(this.globalProvider.existsItemLocalStorage(GlobalProvider.PLANIFICATIONS_CURRENT_KEY)){
      let currentInspection = this.globalProvider.getItemLocalStorage(GlobalProvider.PLANIFICATIONS_CURRENT_KEY);
      if(this.updated){
        let inspection = this.inspections.find(inspection=>{return (currentInspection.inspectionInform.planification_id === inspection.id);})
        if(inspection && inspection.status == "Asignado"){
          this.goViewInspection(inspection, true);
          loading.dismiss();
          return;
        }
      }else{
        this.inspections.find(planification=>{return (planification.id===currentInspection.inspectionInform.planification_id);}).status = 'En proceso';
      }
    }
    inspectionsaux = this.inspections;
    this.isInspectionInProcess = inspectionsaux.some(inspection => {
      return (inspection.status == 'En proceso')
    });
    if(!GlobalProvider.finishedCurrentPlanification){
      this.changeInspectionStatus('Falló la finalización del informe. Reintentando...');
      this.finishCurrentPlanification();
    }
    if(GlobalProvider.MEDIA_ARRAY.length > 0){
      this.changeInspectionStatus(GlobalProvider.finishedQueueUpload? 
        'Falló la subida de algunos archivos. Reintentar?' : 'Enviando archivos');
    }
    loading.dismiss();
  }

  //Una vez cambiado el status ya no encuentra la inspección, ya que su status ya no es En proceso
  returnInspectionInProcess() : any{
    return this.inspections.find(inspection => {
      return (inspection.status == 'En proceso');
    });
  }

  changeInspectionStatus(status : string){
    this.returnInspectionInProcess().status = status;
  }

  finishCurrentPlanification(){
    this.globalProvider.finishPlanification(this.globalProvider.getItemLocalStorage(GlobalProvider.CURRENT_INSPECTION_ID_KEY), true)
    .then(()=>{
      ListPage.updateListPage(this.navCtrl);
    });
  }

  createInspection(inspection:any,request:any, loading, isCreatingInspectionFromStorage? : boolean){
    this.restProvider.postData(GlobalProvider.API_INSPECTION_STORE, request).then((data : any) =>{
      loading.dismiss();
      this.globalProvider.setItemLocalStorage(GlobalProvider.CURRENT_INSPECTION_ID_KEY, data.id);
      inspection.inspection = data;
      if(!isCreatingInspectionFromStorage){
        this.navCtrl.push(InspectionPage, {item:inspection, id:data.id, peoples:inspection.peoples}, {animate: true, direction: 'forward'});
      }else{
        this.navCtrl.setRoot(ListPage, {}, {animate: false});
      }
    }).catch((err)=>{
      console.log("Error de crear inspección", err);
      this.navCtrl.push(InspectionPage, {item:inspection, peoples:inspection.peoples}, {animate: true, direction: 'forward'});
      this.globalProvider.messagesToast("No se pudo crear la inspección, se hará antes de enviar el informe", undefined, undefined, "toast-warning");
      loading.dismiss();
    });
  }

  goViewInspection(inspection, isCreatingInspectionFromStorage? : boolean){
    if(this.isInspectionInProcess){
      if(inspection.status == 'En proceso'){
        console.log("Inspection is in process");
        this.validateLocation(inspection, this.isInspectionInProcess);
      }else if(inspection.status == 'Enviando archivos'){
        this.globalProvider.messagesToast("Enviando archivos del informe.");
      }else if(inspection.status == 'Falló la subida de algunos archivos. Reintentar?'){
        this.alertCtrl.create({
          title: 'No se pudieron subir todos los archivos',
            message: "¿Desea reintentar la carga de archivos?",
            buttons: [
              {
                text: 'No, terminar informe',
                handler: () => {
                  this.finishCurrentPlanification();
                }
              },
              {
                text: 'Si, intentar de nuevo',
                handler: () => {
                  this.globalProvider.uploadInQueueMediaArray(inspection.inspection.id)
                  .then(()=>{
                    ListPage.updateListPage(this.navCtrl);
                  });
                  inspection.status = 'Enviando archivos';
                }
              }
            ]
        }).present();
      }else{
        this.globalProvider.messagesToast("Debe terminar la inspección en proceso antes de comenzar otra.");
      }
    }else{
      if(inspection.status == "Asignado"){

        this.validateLocation(inspection, this.isInspectionInProcess, isCreatingInspectionFromStorage);
      }
    }
  }

  async validateLocation(inspection, isInspectionInProcess : boolean, isCreatingInspectionFromStorage? : boolean){

    let loading = this.globalProvider.preloader();
    loading.present();

    let hasLocationPermission : boolean = await this.permitProvider.requestPermissions(0, "Se debe proveer permisos para poder continuar");

    let isLocationAccracy : boolean = await this.permitProvider.requestForAccuracy(false);

    if (!isLocationAccracy || !hasLocationPermission){
      loading.dismiss();
      return;
    }

    let positionPromise : any;

    if (isLocationAccracy){
      positionPromise = await this.globalProvider.obtainCurrentPosition();
    }

    let isValidLocation : boolean = false;
    let validate : any;
    if (positionPromise.isPosition){
      validate = {
        planification_id : inspection.pivot.planification_id,
        latitude : positionPromise.latitude,
        longitude :  positionPromise.longitude
      }
      /**
       * TODO
       */
      await this.restProvider.postData(GlobalProvider.API_VALIDATION_LOCATION, validate).then((data : any) =>{
        isValidLocation = true;
        positionPromise.ableToValidate = true;
      }).catch(err=>{
        if(err.status === 401){
          positionPromise.ableToValidate = true;
          isValidLocation = false;
        }else{
          positionPromise.ableToValidate = false;
        }
      });
    }

    let requestInpection = {
      planification_id : inspection.pivot.planification_id,
      latitude : positionPromise.latitude,
      longitude :  positionPromise.longitude,
      distance : null,
      time : null,
      total : 0,
      status : "En proceso"
    }
    isValidLocation = isCreatingInspectionFromStorage? true : isValidLocation ;

    if (isValidLocation){
      this.createInspection(inspection,requestInpection,loading, isCreatingInspectionFromStorage);
    } else {
      const prompt = this.alertCtrl.create({
        title: positionPromise.ableToValidate? 'No se encuentra en la ubicación de la obra' : 'No se pudo validar ubicación',
        message: "¿Desea continuar de todas formas?",
        buttons: [
          {
            text: 'Cancelar',
            handler: data => {
              loading.dismiss();
            }
          },
          {
            text: 'Aceptar',
            handler: data => {
              if(isInspectionInProcess){
                loading.dismiss();
                if(inspection.inspection){
                  this.restProvider.putData(GlobalProvider.API_INSPECTION_STORE + '/' + inspection.inspection.id, validate)
                  .catch((err) => {
                    console.log("Error de actualizar inspección", err);
                  });
                  this.globalProvider.setItemLocalStorage(GlobalProvider.CURRENT_INSPECTION_ID_KEY, inspection.inspection.id);
                  this.navCtrl.push(InspectionPage, {item:inspection, id:inspection.inspection.id,peoples:inspection.peoples}, {animate: true, direction: 'forward'});
                }else{
                  this.navCtrl.push(InspectionPage, {item:inspection,peoples:inspection.peoples}, {animate: true, direction: 'forward'});
                }
              }else{
                this.createInspection(inspection,requestInpection,loading, isCreatingInspectionFromStorage);
              }
            }
          }
        ]
      });
      prompt.present();
    }
  }
  
  static updateListPage(navCtrl : NavController){
    if(navCtrl.getActive().instance instanceof ListPage){
      navCtrl.setRoot(ListPage,{},{animate:false});
    } 
  }

  addInspection(){
    this.navCtrl.push(NewPlanificationPage, {animate: true, direction: 'forward'})
  }

}


