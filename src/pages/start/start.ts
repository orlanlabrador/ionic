import { Component } from '@angular/core';
import { MenuController, NavController, Platform } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { LoginPage } from '../login/login';
import { RestProvider } from '../../providers/rest/rest';
import { RegisterPage } from '../register/register';

@Component({
  selector: 'page-start',
  templateUrl: 'start.html',
})
export class StartPage {

  constructor(public global : GlobalProvider,
              public restProvider: RestProvider,
              public menuCtrl : MenuController,
              public navCtrl : NavController,
              public platform : Platform) {
    this.ChangeBackButton();
    this.menuCtrl.enable(false, 'MenuObras');
  }

  goLogin(){
    this.navCtrl.push(LoginPage);
  }

  ChangeBackButton(){
    this.platform.registerBackButtonAction(() => {         
    navigator['app'].exitApp();
  });
  }

  goRegisterWithEnterprisesAndPeopleTypes(){
    let loading = this.global.preloader();
    loading.present();
    this.restProvider.getData(GlobalProvider.API_ENTERPRISES)
    .then((enterprises:any) => {
        this.restProvider.getData(GlobalProvider.API_PEOPLE_TYPES)
        .then((peopleTypes:any) => {
          loading.dismiss();
          let peopleTypes_current = peopleTypes.peopletypes.filter(ele=>{
            return ele.isInternal != 1;
          })
          this.navCtrl.push(RegisterPage, {enterprises : enterprises.enterprises, peopleTypes : peopleTypes_current}, {animate: true, direction: 'forward'})
        })
        .catch((err)=>{
          this.showToast("Servicio temporalmente no disponible", "toast-error")
          console.log("CATCH ERROR DATA URL",err);
          loading.dismiss();
      });
    })
    .catch((err)=>{
        this.showToast("Servicio temporalmente no disponible", "toast-error")
        console.log("CATCH ERROR DATA URL",err);
        loading.dismiss();
    })
  }

  showToast(message : string, type : string){
    this.global.messagesToast(message, undefined, undefined, type)
  }
}
