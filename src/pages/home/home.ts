import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, App, MenuController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { ListPage } from '../list/list';
import { DesertionPage } from "../desertion/desertion";
import { EventualityPage } from "../eventuality/eventuality";
import { WorkPage } from "../work/work";
import { GlobalProvider } from '../../providers/global/global';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('map') mapElement: ElementRef;
  //map: any;
  customColor:any;
  runner = {}
  trasscoords = []
  interval
  typeUser: string = '';
  userInfo : any = {};
  isSupervisor: boolean;
  updateUserDataSub: Subscription;
  constructor(public navCtrl: NavController,
              public platform : Platform,
              public app : App,
              public menuCtrl: MenuController,
              public globalProvider: GlobalProvider) {
    this.customColor="#009EE3";
    //this.testMethod();
    this.ChangeBackButton();
    this.menuCtrl.enable(true,'MenuObras');
    this.init();
  }

  testMethod(){
    this.globalProvider.promiseToTest(false)
    .then(valueOfPromise => {
      console.log("Value if promise gets resolved", valueOfPromise);
    })
    .catch(err => {
      console.log("Value if promise gets rejected", err);
    });
  }

  async init(){
    this.userInfo = this.globalProvider.getItemLocalStorage(GlobalProvider.PEOPLE_KEY);
    this.typeUser = this.userInfo.type;
    this.isSupervisor = this.userInfo.people_types.some(peopleType => {
      return peopleType.isSupervisor == 1;
    });
    this.updateUserDataSub = this.globalProvider.updateUserData().subscribe(result => {
      console.log("Resultado del observable", result);
      this.globalProvider.setLocalStorageData(result);
    });;
  }

  ionViewWillLeave(){
    this.updateUserDataSub.unsubscribe();
  }

  ChangeBackButton(){
    this.platform.registerBackButtonAction(() => {         
    navigator['app'].exitApp();
  });
  }

  goView(viewName){
    switch(viewName) {
      case 'inspecciones':
        this.navCtrl.setRoot(ListPage, {}, {animate: true, direction: 'forward'});
        break;
      case 'informes':
        this.navCtrl.setRoot(EventualityPage, {}, {animate: true, direction: 'forward'});
        break;
      case 'ajustes':
        this.navCtrl.setRoot(DesertionPage, {}, {animate: true, direction: 'forward'});
        break;
      case 'obras':
        this.navCtrl.setRoot(WorkPage, {}, {animate: true, direction: 'forward'});
        break;                
    }
  }

}