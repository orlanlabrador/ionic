import { RestProvider } from './../../providers/rest/rest';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Platform, App } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GlobalProvider } from '../../providers/global/global';
import { Subscription } from 'rxjs/Subscription';
import { IonicSelectableComponent } from 'ionic-selectable';

@Component({
  selector: 'page-new-planification',
  templateUrl: 'newPlanification.html',
})
export class NewPlanificationPage {
  @ViewChild('selectSearchComponent') selectSearchComponent: IonicSelectableComponent;

  userInfo : any = {};
  myForm: FormGroup;
  email : string;

  latitude : number;
  longitude : number;

  constructions_confirmed : any[] = []
  protocols : any[] = []

  participants : any = {}

  peoples_selected : any[] = []

  peoples : any[] =[]

  map: any;

  minDate : string;
  maxDate : string;
  validTime : any = {
    validDates : true,
    validHour : true
  };
  subsToValidateDatesStartDate: Subscription;
  subsToValidateDatesEndDate: Subscription;
  subsToValidateStartHour: any;
  subsToValidateDatesEndHour: any;
  enterprises: any;
  dataDidLoad : boolean;
  enterprisesNames: any[] = [];
  enterpriseIsSet: boolean;
  enterpriseid: string;
  supervisors: any[] = [];
  rolSelected: any;
  participantSelected: {};
  peoplesIsSet: boolean;
  supervisorRoles: any[] = [];
  rolSupervisorSelected: any;
  participantsIsSet: boolean;
  rolSelectedAux: any;
  rolSupervisorSelectedAux: any;
  canCreatePlan: boolean;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public fb: FormBuilder,
              public restProvider: RestProvider,
              public platform : Platform,
              public app : App,
              public globalProv : GlobalProvider) {
    this.dataDidLoad = false;
    this.canCreatePlan = true;
    this.enterpriseIsSet = false;
    this.peoplesIsSet = false;
    this.participantsIsSet = false;
    this.init();
  }

  crearFormulario(){
    return this.fb.group({
      description: ['', [Validators.required]],
      construction_id: ['', [Validators.required]],
      enterprise:['',[Validators.required]],
      protocol_id: ['', [Validators.required]],
      start_date: ['', [Validators.required]],
      start_hour: ['', [Validators.required]],
      end_date: ['', [Validators.required]],
      end_hour: ['', [Validators.required]],
      supervisor: ['',[Validators.required]]
    });
  }

  ionViewWillLeave(){
    if(this.dataDidLoad){
      this.subsToValidateDatesStartDate.unsubscribe();
      this.subsToValidateDatesEndDate.unsubscribe();
      this.subsToValidateStartHour.unsubscribe();
      this.subsToValidateDatesEndHour.unsubscribe();
    }
  }

  async init(){
    let loading = this.globalProv.preloader();
    loading.present();

    await this.restProvider.getData(GlobalProvider.API_PEOPLE + this.globalProv.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).id)
    .then((data : any) => {
      this.globalProv.setItemLocalStorage(GlobalProvider.PEOPLE_KEY, data.people);
    })
    .catch(err => {
      this.globalProv.messagesToast('No se pudo actualizar la información de usuario.', undefined, undefined, "toast-warning");
    });

    await this.restProvider.getData(GlobalProvider.API_ENTERPRISES)
    .then((data : any) => {
      this.enterprises = data.enterprises.filter(enterprise => {
        return (enterprise.id != 2);
      });
    })
    .catch(err => {
      this.canCreatePlan = false;
    });

    await this.restProvider.getData(GlobalProvider.API_PROTOCOLS)
    .then((data:any) => {
      this.protocols = data.protocols;
    })
    .catch(err => {
      this.canCreatePlan = false;
    });
    
    this.userInfo = this.globalProv.getItemLocalStorage(GlobalProvider.PEOPLE_KEY);
    this.supervisorRoles = this.userInfo.people_types.filter(rol => {
      return (rol.isSupervisor === 1);
    });

    [this.minDate, this.maxDate] = this.globalProv.CurrentAndNextdate();
    this.myForm = this.crearFormulario();
    if(!this.canCreatePlan){
      this.myForm.disable();
    }
    this.notAvailable();
    if(this.userInfo.enterprise_id != 2){
      this.myForm.controls['enterprise'].setValue(this.userInfo.enterprise_id);
      this.enterpriseSelected( {} , this.userInfo.enterprise_id);
    }
    [this.subsToValidateDatesStartDate, this.subsToValidateDatesEndDate,this.subsToValidateStartHour, this.subsToValidateDatesEndHour] = this.globalProv.changeIniDateAndSetFinDate(this.myForm, ['start_date','end_date','start_hour','end_hour'], this.validTime, true);

    this.peoples_selected = [];
    this.participants.roles = this.globalProv.getItemLocalStorage(GlobalProvider.PEOPLE_TYPE_KEY).filter(rol => {
      return rol.isInternal === 0;
    });

    this.dataDidLoad = true;

    loading.dismiss();
  }

  removeSupervisor(){

    if(this.peoples_selected.find(people=>{return (people.selected_peopleType.id===this.myForm.get('supervisor').value.id);})){
      this.myForm.controls['supervisor'].setValue(this.rolSupervisorSelectedAux);
      this.globalProv.messagesToast("Este rol ya lo tiene un participante");
    }else{
      if(this.rolSelected){
        if(this.rolSelected.id===this.myForm.get('supervisor').value.id){
          this.myForm.controls['supervisor'].setValue(this.rolSupervisorSelectedAux);
          this.globalProv.messagesToast("Este rol ya esta en rol de participante");
        }else{
          if(this.rolSupervisorSelectedAux){
            this.participants.roles.push(this.rolSupervisorSelectedAux);
          }
          this.rolSupervisorSelectedAux = this.myForm.get('supervisor').value;
          this.participants.roles.splice(this.participants.roles.findIndex(rol=>{return (rol.id===this.myForm.get('supervisor').value.id);}), 1);
        }
      }else{
        if(this.rolSupervisorSelectedAux){
          this.participants.roles.push(this.rolSupervisorSelectedAux);
        }
        this.rolSupervisorSelectedAux = this.myForm.get('supervisor').value;
        this.participants.roles.splice(this.participants.roles.findIndex(rol=>{return (rol.id===this.myForm.get('supervisor').value.id);}), 1);
        this.participantsIsSet = true;
      }
    }
  }

  onSelect(event: {
    component: IonicSelectableComponent,
    item: any,
    isSelected: boolean
  }) {
    let selected = event.item;
    selected.selected_peopleType = this.rolSelected;
    this.peoples_selected.push(selected);
    this.peoples.splice(this.peoples.findIndex(people=>{return (people.id===selected.id);}), 1);
    this.participants.roles.splice(this.participants.roles.findIndex(rol=>{return (rol.id===this.rolSelected.id);}), 1);
    this.rolSelected = {};
    this.clearSelectSearch();
  }

  onClose(event: { component: IonicSelectableComponent }) {
    this.participantSelected = {};
    this.clearSelectSearch();
  }

  clearSelectSearch() {
    this.selectSearchComponent.clear();
  }

  removeSelected(people_selected : any){
    this.participants.roles.push(people_selected.selected_peopleType);
    delete people_selected.selected_peopleType;
    this.peoples.push(people_selected);
    this.peoples_selected.splice(this.peoples_selected.findIndex((people=>{return (people.id===people_selected.id);})), 1);
  }

  notAvailable(){
    if(this.myForm.disabled){
      this.globalProv.messagesToast("Servicio temporalmente no disponible");
      return;
    }
  }

  async enterpriseSelected(event: {
    component?: IonicSelectableComponent,
    value?: any 
  }, enterprise_id? : string) {

    let loading = this.globalProv.preloader("Cargando información...");
    if(!enterprise_id){
      loading.present();
    }

    this.enterpriseid = enterprise_id? enterprise_id : event.value.id ;

    //Información de Minera Luren
    await this.restProvider.getData(GlobalProvider.API_ENTERPRISE_DATA + '1')
    .then((data : any) => {
      this.peoples = data.enterprise.persons;
    })
    .catch(err => {
      this.myForm.disable();
      console.log(err);
    });

    //Información de la empresa seleccionada
    await this.restProvider.getData(GlobalProvider.API_ENTERPRISE_DATA + this.enterpriseid)
    .then((data : any) => {
      this.enterpriseIsSet = true;
      this.constructions_confirmed = data.enterprise.constructions.filter(construct => {
        return (construct.status === "Confirmada" && construct.hasPlanification === 0);
      });
      data.enterprise.persons.forEach(person => {
        this.peoples.push(person);
      });
      console.log("Resultado de la petición de la data de la empresa", data);
    })
    .catch(err => {
      this.myForm.disable();
      console.log(err);
    });

    //Información de empresa "independiente"
    await this.restProvider.getData(GlobalProvider.API_ENTERPRISE_DATA + '2')
    .then((data : any) => {
      data.enterprise.persons.forEach(person => {
        this.peoples.push(person);
      });
      this.peoplesIsSet = true;
    })
    .catch(err => {
      this.myForm.disable();
      console.log(err);
    });

    this.peoples.splice(this.peoples.findIndex(people=>{return (people.id===this.userInfo.id);}), 1);

    this.notAvailable();

    this.participants.names = [];

    if(this.myForm.get('construction_id').value){
      this.myForm.controls['construction_id'].reset({});
      this.myForm.controls['construction_id'].setErrors({"required":true});
    }

    if(this.peoples_selected.length > 0){
      this.participants.roles = this.globalProv.getItemLocalStorage(GlobalProvider.PEOPLE_TYPE_KEY).filter(rol => {
        return (rol.isInternal === 0 && rol.id!==this.myForm.get('supervisor').value.id);
      });
      this.peoples_selected = [];
    }

    if(!enterprise_id){
      loading.dismiss();
    }
  }

  searchNames() {
    this.rolSelectedAux = this.rolSelected;
    this.participants.names = [];
    this.peoples.forEach(people => {
      if (people.people_types.find((people_type:any) => {
        return people_type.id == this.rolSelectedAux.id;
      })){
        people.name = people.first_name + ' ' + people.last_name;
        this.participants.names.push(people);
      }
    });
  }

  back(){
    this.navCtrl.pop({animate: true, direction: 'backward'})
  }

  savePlanification(){
    let loading = this.globalProv.preloader()
    loading.present();

    let newPlanification = {
      protocol_id : this.myForm.value.protocol_id.id,
      enterprise_id : this.enterpriseid,
      construction_id : this.myForm.value.construction_id.id,
      description : this.myForm.value.description,
      start_date : this.myForm.value.start_date,
      start_hour : this.myForm.value.start_hour,
      end_date : this.myForm.value.end_date,
      end_hour : this.myForm.value.end_hour,
      status : "Asignado",
      leader: this.userInfo.id,
      selected : [
        {
          peopleTypes : {
            id : this.myForm.value.supervisor.id
          },
          peoples : {
            id : this.userInfo.id
          }
        }
      ]
    }
    
    this.peoples_selected.forEach(element => {
      newPlanification.selected.push({
        peopleTypes : {
          id : element.selected_peopleType.id
        },
        peoples : {
          id : element.id
        }
      })
    });

    console.log("New planification to send", newPlanification);
    
    this.restProvider.postData(GlobalProvider.API_PLANIFICATION_CRUD, newPlanification)
    .then((data:any) => {
      this.globalProv.messagesToast("Se creó la planificación", undefined, undefined, "toast-success")
      loading.dismiss();
      this.navCtrl.pop({animate : true, direction: 'backward'});
    })
    .catch((err)=>{
      console.log(err)
      this.globalProv.messagesToast('Servicio temporalmente no disponible', undefined, undefined, "toast-error")
      loading.dismiss();
    });

  }
}
