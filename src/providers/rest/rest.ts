import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class RestProvider {

    //static BASE_URL_SERVER : string = "http://174.138.33.157:8080";

    //static BASE_URL_SERVER : string = "http://app.lacasa.pe:9090";

    static BASE_URL_SERVER : string = "https://app.lacasa.pe";

    //static BASE_URL_SERVER : string = "http://192.168.20.143";

    //static BASE_URL_SERVER : string = "http://192.168.1.2";

    static API_URL : string = RestProvider.BASE_URL_SERVER + '/api/';

    header = new HttpHeaders();

    constructor(public http: HttpClient) {
    }

    /**
     *
     * Metodo para el consumo de apis get
     *
     * @param metodo : string nombre del metodo a consumir
     *
     */
    getData(metodo, params="", observable? : boolean){
        let returnType : any;
        let headers = new HttpHeaders({"Content-Type": "application/json"});
        let uri = RestProvider.API_URL + metodo + params;
        if(observable){
            returnType = this.http.get(uri, {headers: headers});
        }else{
            returnType = this.http.get(uri, {headers: headers}).toPromise();
        }
        return returnType;
    }

    /**
     *
     * Metodo para el consumo de apis post
     *
     * @param metodo : string nombre del metodo a consumir
     *
     */
    postData(metodo, params=null){
        let headers;
        let api;
        headers = new HttpHeaders({"Content-Type": "application/json"});
        api = RestProvider.API_URL;
        return this.http.post(api + metodo, params, {headers:headers}).toPromise();
    }

    /**
     *
     * Metodo para el consumo de apis post
     *
     * @param metodo : string nombre del metodo a consumir
     *
     */
    postDataFile(metodo, params=null){
        let headers;
        let api;
        headers = new HttpHeaders({"Content-Type": "multipart/form-data"});
        api = RestProvider.API_URL;
        return this.http.post(api + metodo, params, {headers:headers}).toPromise();
    }

    /**
     *
     * Metodo para el consumo de apis put
     *
     * @param metodo : string nombre del metodo a consumir
     *
     */
    putData(metodo, params=null){
        let headers;
        let api;
        headers = new HttpHeaders({"Content-Type": "application/json"});
        api = RestProvider.API_URL;
        return this.http.put(api + metodo, params, {headers:headers}).toPromise();
    }

    /**
     *
     * Metodo para el consumo de apis delete
     *
     * @param metodo : string nombre del metodo a consumir
     *
     */
    deleteData(metodo, params=''){
        let headers = new HttpHeaders({"Content-Type": "application/json"});
        let uri = RestProvider.API_URL + metodo + params;
        return this.http.delete(uri, {headers: headers}).toPromise();
    }

}