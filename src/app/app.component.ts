import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { DesertionPage } from "../pages/desertion/desertion";
import { EventualityPage } from "../pages/eventuality/eventuality";
import { TermConditionsPage } from '../pages/termConditions/termConditions';
import { WorkPage } from './../pages/work/work';
import { GlobalProvider } from '../providers/global/global';
import { StartPage } from '../pages/start/start';
import { RestProvider } from '../providers/rest/rest';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  pages: Array<{title: string, component: any, icon:string}>;
  static _user : any = {
    name:'',
    email:'',
    type:'',
    urlPhoto:'',
    isSupervisor:false,
    imgLoaded:false
  };
  id: any;
  photo_name: any;
  urlPhoto: string;
  userInfo: any;
  isSupervisor: any;
 
  constructor(public platform: Platform,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              public globalProv: GlobalProvider,
              public alertCtrl: AlertController,
              public menuCtrl: MenuController,
              public RestProvider: RestProvider) {
    this.initializeApp();
    this.pages = [
      { title: 'Inicio', component: HomePage, icon:"home" },
      { title: 'Inspecciones', component: ListPage, icon:"filing" },
      { title: 'Informes', component: EventualityPage,  icon:"notifications"},
      { title: 'Perfil', component: DesertionPage,  icon:"log-out"},
      { title: 'Obras', component: WorkPage, icon:"building"}
    ];
  }

  loadEnded(){
    MyApp._user.imgLoaded = true;
  }

  get staticUser() {
    return MyApp._user;
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();
      this.splashScreen.hide();
      GlobalProvider.APP_DIRECTORY_PATH_INTERN_MEMORY = this.globalProv.file.externalRootDirectory + GlobalProvider.APP_DIRECTORY_INTERN_MEMORY + '/';
      this.globalProv.createDirectory(this.globalProv.file.externalRootDirectory,GlobalProvider.APP_DIRECTORY_INTERN_MEMORY);
      window.addEventListener('keyup', (event) => {
        if (event.which === 13) {
          this.globalProv.hideKeyboard();
        }
      });
      if (this.globalProv.existsItemLocalStorage(GlobalProvider.USER_KEY)){
        this.globalProv.updateUserInfo();
        this.rootPage = HomePage;
      }else{
        if (this.globalProv.existsItemLocalStorage(GlobalProvider.HAS_USER_AGREED_KEY)){
          this.rootPage = StartPage;
        } else {
          this.rootPage = TermConditionsPage;
        }
      }
    });
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }

  logOut(){
    if(this.globalProv.existsItemLocalStorage(GlobalProvider.MEDIA_ARRAY_KEY)){
      this.globalProv.messagesToast("No puede cerrar sesión mientras hay imágenes por enviar", undefined, undefined, "toast-warning");
    }else if(this.globalProv.existsItemLocalStorage(GlobalProvider.PLANIFICATIONS_CURRENT_KEY)){
      this.alertCtrl.create({
        title: 'Hay una planificación en curso',
          message: "¿Seguro que desea salir (se borrará el progreso)?",
          buttons: [
            {
              text: 'Cancelar',
            },
            {
              text: 'Si, cerrar sesión',
              handler: () => {
                this.closeSession();
              }
            }
          ]
      }).present();
    }else{
      this.closeSession();
    }
  }

  closeSession(){
    localStorage.clear();
    this.globalProv.setItemLocalStorage(GlobalProvider.HAS_USER_AGREED_KEY,{value:true});
    this.nav.setRoot(StartPage);
  }
}