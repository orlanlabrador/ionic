import { Geolocation } from '@ionic-native/geolocation';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DesertionPage } from "../pages/desertion/desertion";
import { EventualityPage } from "../pages/eventuality/eventuality";
import { RegisterPage } from '../pages/register/register';
import { InspectionPage } from '../pages/inspection/inspection';
import { InspectionDetailsPage } from '../pages/inspectionDetails/inspectionDetails';
import { InspectionInformPage } from '../pages/inspectionInform/inspectionInform';
import { TermConditionsPage } from '../pages/termConditions/termConditions';
import { WorkPage } from '../pages/work/work';
import { EditworkPage } from '../pages/editwork/editwork';
import { HttpClientModule } from '@angular/common/http';
import { RestProvider } from '../providers/rest/rest';
import { GlobalProvider } from '../providers/global/global';
import { LoginPage } from '../pages/login/login';
import { RecoveryPasswordPage } from '../pages/recoveryPassword/recoveryPassword';
import { StartPage } from '../pages/start/start';
import { NewWorkPage } from '../pages/newWork/newWork';
import { Camera } from '@ionic-native/camera';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { NewPlanificationPage } from '../pages/newPlanification/newPlanification';
import { CommentPage } from '../pages/comment/comment';
import { PreviewInspectionPage } from '../pages/previewInspection/previewInspection';
import { Keyboard } from '@ionic-native/keyboard';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Downloader } from '@ionic-native/downloader';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { IonicSelectableModule } from 'ionic-selectable';
import { IonicImageLoader } from 'ionic-image-loader';
import { PermissionProvider } from '../providers/permissions/permissions';
//import { SocialSharing } from '@ionic-native/social-sharing';

@NgModule({
  declarations: [
    MyApp,
    StartPage,
    RegisterPage,
    LoginPage,
    RecoveryPasswordPage,
    HomePage,
    RecoveryPasswordPage,
    ListPage,
    DesertionPage, 
    EventualityPage, 
    InspectionPage,
    InspectionDetailsPage,
    TermConditionsPage,
    NewPlanificationPage,
    InspectionInformPage,
    WorkPage,
    NewWorkPage,
    EditworkPage,
    CommentPage,
    PreviewInspectionPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicSelectableModule,
    IonicImageLoader.forRoot(),
    IonicModule.forRoot(MyApp,{
        scrollPadding: false,
        scrollAssist: false    
        }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    StartPage,
    RegisterPage,
    LoginPage,
    HomePage,
    RecoveryPasswordPage,
    ListPage,
    DesertionPage, 
    EventualityPage, 
    InspectionPage,
    InspectionDetailsPage,
    TermConditionsPage,
    NewPlanificationPage,
    InspectionInformPage,
    WorkPage,
    NewWorkPage,
    EditworkPage,
    CommentPage,
    PreviewInspectionPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GlobalProvider,
    RestProvider,
    PermissionProvider,
    Camera,
    Geolocation,
    FilePath,
    File,
    FileTransfer,
    FileTransferObject,
    Keyboard,
    AndroidPermissions,
    Downloader,
    LocationAccuracy,
    //SocialSharing
  ]
})
export class AppModule {}
